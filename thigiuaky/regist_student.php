<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        form {
            width: 600px; /* Increased width for better spacing */
            padding: 20px;
            border: 2px solid rgb(48, 113, 178);
        }

        .c {
            display: flex;
            flex-wrap: wrap;
            gap: 10px;
            padding: 10px;
        }

        .l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 4px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
            text-align: center;
        }

        .i-c {
            flex: 2;
        }

        .i-t, .s {
            width: 100%;
            padding: 6px;
            margin-top: 5px;
            border: 2px solid rgb(48, 113, 178);
            position: relative;
        }

        .i-t::after,
        .s::after {
            content: "▼"; /* Triangle icon */
            position: absolute;
            top: 50%;
            right: 10px;
            transform: translateY(-50%);
        }

        .c-s {
            position: relative;
        }

        .c-s select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            width: 100%;
            padding: 8px;
            border: 2px solid rgb(48, 113, 178);
            position: relative;
        }

        .a {
            position: absolute;
            top: 0;
            right: 0;
            width: 30px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: rgb(118, 178, 93);
            color: white;
            cursor: pointer;
        }

        .g-c {
            display: flex;
            gap: 10px;
            padding: 10px;
        }

        .g-l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 4px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            text-align: center;
        }

        .g-i-c {
            flex: 2;
            display: flex;
            align-items: center;
        }

        .g-i-c label {
            margin-right: 5px;
        }

        .b-c {
            text-align: center;
            margin-top: 20px;
        }

        .b {
            padding: 10px 30px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
            color: white;
            border: 2px solid rgb(48, 113, 178);
        }

        .b:hover {
            background-color: rgb(24, 87, 182);
        }

        .min {
            flex: 1;
            padding: 6px;
            height: 41px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .max {
            flex: 1;
            padding: 6px;
            width: 135px;
            height: 60px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space_between;
            align-items: center;
        }

        .img {
            flex: 1.17;
        }

        .required {
            color: red;
        }

        .error-message {
            color: red;
        }

        .hinh {
            flex: 2;
            padding: 6px;
            float: left;
            margin-right: 125px;
        }

        .error-message {
            text-align: center;
            margin-bottom: 10px;
            color: red;
        }
    </style>
</head>

<body>
    <div>
        <?php
        // Check if the form was submitted
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // Retrieve and display the submitted data
            $name = $_POST["name"];
            $gender = $_POST["gender"];
            $birthdate = $_POST["year"] . "-" . $_POST["month"] . "-" . $_POST["day"];
            $city = $_POST["city"];
            $district = $_POST["district"];
            $other_info = $_POST["other_info"];

            echo "<p><strong>Họ và Tên:</strong> $name</p>";
            echo "<p><strong>Giới tính:</strong> " . ($gender == 1 ? "Nam" : "Nữ") . "</p>";
            echo "<p><strong>Ngày sinh:</strong> $birthdate</p>";
            echo "<p><strong>Thành phố:</strong> $city</p>";
            echo "<p><strong>Quận:</strong> $district</p>";
            echo "<p><strong>Thông tin khác:</strong> $other_info</p>";
        } else {
            echo "<p>No data submitted.</p>";
        }
        ?>
    </div>
</body>
</html>

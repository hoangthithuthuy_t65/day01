<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">

    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        form {
            width: 600px; /* Increased width for better spacing */
            padding: 20px;
            border: 2px solid rgb(48, 113, 178);
        }

        .c {
            display: flex;
            flex-wrap: wrap;
            gap: 10px;
            padding: 10px;
        }

        .l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 4px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
            text-align: center;
        }

        .i-c {
            flex: 2;
        }

        .i-t, .s {
            width: 100%;
            padding: 6px;
            margin-top: 5px;
            border: 2px solid rgb(48, 113, 178);
            position: relative;
        }

        .i-t::after,
        .s::after {
            content: "▼"; /* Triangle icon */
            position: absolute;
            top: 50%;
            right: 10px;
            transform: translateY(-50%);
        }

        .c-s {
            position: relative;
        }

        .c-s select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            width: 100%;
            padding: 8px;
            border: 2px solid rgb(48, 113, 178);
            position: relative;
        }

        .a {
            position: absolute;
            top: 0;
            right: 0;
            width: 30px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: rgb(118, 178, 93);
            color: white;
            cursor: pointer;
        }

        .g-c {
            display: flex;
            gap: 10px;
            padding: 10px;
        }

        .g-l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 4px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            text-align: center;
        }

        .g-i-c {
            flex: 2;
            display: flex;
            align-items: center;
        }

        .g-i-c label {
            margin-right: 5px;
        }

        .b-c {
            text-align: center;
            margin-top: 20px;
        }

        .b {
            padding: 10px 30px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
            color: white;
            border: 2px solid rgb(48, 113, 178);
        }

        .b:hover {
            background-color: rgb(24, 87, 182);
        }

        .min {
            flex: 1;
            padding: 6px;
            height: 41px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .max {
            flex: 1;
            padding: 6px;
            width: 135px;
            height: 60px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space_between;
            align-items: center;
        }

        .img {
            flex: 1.17;
        }

        .required {
            color: red;
        }

        .error-message {
            color: red;
        }

        .hinh {
            flex: 2;
            padding: 6px;
            float: left;
            margin-right: 125px;
        }

        .error-message {
            text-align: center;
            margin-bottom: 10px;
            color: red;
        }
    </style>
</head>

<body>
    <form action="regist_student.php" method="post">

        <div class="error-message" style="display: none;"></div>

        <div class="c">
            <div class="l" style="max-width: 90px;">Họ và tên<span class="required">*</span></div>
            <div class="i-c">
                <input type="text" class="s" name="name">
            </div>
        </div>

        <div class="c">
            <div class="l" style="max-width: 90px;">Giới tính<span class="required">*</span></div>
            <div class="i-c">
                <label><input type="radio" name="gender" value="1"> Nam</label>
                <label><input type="radio" name="gender" value="2"> Nữ</label>
            </div>
        </div>

        <div class="c c-s">
            <div class="l" style="max-width: 90px;">Ngày Sinh<span class="required">*</span></div>
            <div class="i-c" style="display: flex; gap: 5px;">
                <select name="year" style="flex: 1;">
                <option value="">Năm</option>
                    <?php
                    $currentYear = date("Y");
                    for ($year = $currentYear - 40; $year <= $currentYear - 15; $year++) {
                        echo "<option value='$year'>$year</option>";
                    }
                    ?>
                </select>

                <select name="month" style="flex: 1;">
                <option value="">Tháng</option>
                    <?php
                    for ($month = 1; $month <= 12; $month++) {
                        echo "<option value='$month'>$month</option>";
                    }
                    ?>
                </select>

                <select name="day" style="flex: 1;">
                <option value="">Ngày</option>
                    <?php
                    for ($day = 1; $day <= 31; $day++) {
                        echo "<option value='$day'>$day</option>";
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="c">
            <div class="l" style="max-width: 90px;">Địa chỉ<span class="required">*</span></div>
            <div class="i-c" style="display: flex; gap: 5px;">
                <div class="c-s" style="flex: 1;">
                    <select name="city">
                        <option value="">Thành Phố</option>
                        <option value="Hà Nội">Hà Nội</option>
                        <option value="Tp.Hồ Chí Minh">Tp.Hồ Chí Minh</option>
                    </select>
                </div>
                <div class="c-s" style="flex: 1;">
                    <select name="district" id="districtSelect" disabled>
                        <option value="">Quận</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="c">
            <div class="l" style="max-width: 90px;">Thông tin khác</div>
            <div class="i-c">
                <textarea name="other_info" class="s" rows="4" placeholder="Nhập thông tin khác"></textarea>
            </div>
        </div>

        <div class="b-c">
            <button type="submit" class="b">Đăng ký</button>
        </div>
    </form>

    <script>
        document.querySelector('select[name="city"]').addEventListener('change', function () {
            const selectedCity = this.value;
            const districtSelect = document.querySelector('#districtSelect');
            districtSelect.disabled = selectedCity === '';
            if (selectedCity === 'Hà Nội') {
                districtSelect.innerHTML = `
                    <option value="Hoàng Mai">Hoàng Mai</option>
                    <option value="Thanh Trì">Thanh Trì</option>
                    <option value="Nam Từ Liêm">Nam Từ Liêm</option>
                    <option value="Hà Đông">Hà Đông</option>
                    <option value="Cầu Giấy">Cầu Giấy</option>
                `;
            } else if (selectedCity === 'Tp.Hồ Chí Minh') {
                districtSelect.innerHTML = `
                    <option value="Quận 1">Quận 1</option>
                    <option value="Quận 2">Quận 2</option>
                    <option value="Quận 3">Quận 3</option>
                    <option value="Quận 7">Quận 7</option>
                    <option value="Quận 9">Quận 9</option>
                `;
            }
            else {
                // Reset district options
                districtSelect.innerHTML = '<option value="">Chọn Quận</option>';
            }
        });    
        </script>

    <script>
    document.addEventListener("DOMContentLoaded", function() {
        document.querySelector("form").addEventListener("submit", function(event) {
            var errors = [];
            var name = document.querySelector("input[name='name']").value;
            var gender = document.querySelector("input[name='gender']:checked");
            var year = document.querySelector("select[name='year']").value;
            var month = document.querySelector("select[name='month']").value;
            var day = document.querySelector("select[name='day']").value;
            var city = document.querySelector("select[name='city']").value;

            if (name === "") {
                errors.push("Hãy nhập Họ tên.");
            }

            if (!gender) {
                errors.push("Hãy chọn Giới tính.");
            }

            if (year === "" || month === "" || day === "") {
                errors.push("Hãy chọn Ngày sinh.");
            }

            if (city === "") {
                errors.push("Hãy chọn Địa chỉ.");
            }

            if (errors.length > 0) {
                event.preventDefault();
                var errorMessage = errors.join("<br>");
                var errorMessageElement = document.querySelector(".error-message");
                errorMessageElement.innerHTML = errorMessage;
                errorMessageElement.style.display = "block";
            }
        });
    });
    </script>
</body>
</html>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Xác nhận thông tin đăng ký</title>
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        form {
            width: 550px;
            padding: 40px;
            border: 2px solid rgb(48, 113, 178);
        }

        .c {
            display: flex;
            flex-wrap: wrap;
            gap: 20px;
            padding: 10px;
        }

        .l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 8px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
            text-align: center;
        }

        .i-c {

            flex: 2;
        }

        .i-t,
        .s {
            width: 100%;
            padding: 8px;
            margin-top: 5px;
        }

        .c-s {
            position: relative;
        }

        .c-s select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            width: 100%;
            padding: 10px;
            border: 2px solid rgb(48, 113, 178);
        }

        .a {
            position: absolute;
            top: 0;
            right: 0;
            width: 40px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: rgb(118, 178, 93);
            color: white;
            cursor: pointer;
        }


        .g-c {
            display: flex;
            gap: 20px;
            padding: 10px;
        }

        .g-l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 8px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            text-align: center;
        }

        .g-i-c {
            flex: 2;
            display: flex;
            align-items: center;
        }

        .g-i-c label {
            margin-right: 10px;
        }

        .b-c {
            text-align: center;
            margin-top: 20px;
        }

        .b {
            padding: 15px 50px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
            color: white;
            border: 2px solid rgb(48, 113, 178);
        }

        .b:hover {
            background-color: rgb(24, 87, 182);
        }

        .min {
            flex: 1;
            padding: 8px;
            height: 41px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .max {
            flex: 1;
            padding: 8px;
            width: 315px;
            height: 80px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .img {
            flex: 1.17;
        }

        .required {
            color: red;
        }

        .error-message {
            color: red;
        }

        .hinh {
            flex: 2;
            padding: 8px;
            float: left;
            margin-right: 175px;

        }
    </style>
</head>


<body>
    <form>
        <div class="c">
            <div class="l" style="max-width: 110px;">Họ và tên<span class="required">*</span></div>
            <div class="i-c">
                <?php
                if (isset($_POST['name'])) {
                    $name = $_POST['name'];
                    echo '<input type="text" class="s" value="' . $name . '" readonly>';
                }
                ?>
            </div>
        </div>

        <div class="c">
            <div class="l" style="max-width: 110px;">Giới tính<span class="required">*</span></div>
            <div class="i-c">
                <?php
                if (isset($_POST['gender'])) {
                    $gender = $_POST['gender'];
                    echo '<input type="text" class="s" value="' . $gender . '" readonly>';
                }
                ?>
            </div>
        </div>

        <div class="c c-s">
            <div class="l" style="max-width: 110px;">Phân khoa<span class="required">*</span></div>
            <div class="i-c">
                <?php
                if (isset($_POST['department'])) {
                    $department = $_POST['department'];
                    echo '<input type="text" class="s" value="' . $department . '" readonly>';
                }
                ?>
            </div>
        </div>

        <div class="c">
            <div class="l" style="max-width: 110px;">Ngày Sinh<span class="required">*</span></div>
            <div class="i-c">
                <?php
                if (isset($_POST['ngay-sinh'])) {
                    $ngay_sinh = $_POST['ngay-sinh'];
                    echo '<input type="text" class="s" value="' . $ngay_sinh . '" readonly>';
                }
                ?>
            </div>
        </div>

        <div class="c">
            <div class="l" style="max-width: 110px;">Địa chỉ</div>
            <div class="i-c">
                <?php
                if (isset($_POST['dia-chi'])) {
                    $dia_chi = $_POST['dia-chi'];
                    echo '<input type="text" class="s" value="' . $dia_chi . '" readonly>';
                }
                ?>
            </div>
        </div>

        <div class="c">
            <div class="img">
                <div class="l" style="max-width: 110px;">Hình ảnh</div>
            </div>
            <div class="i-c">
                <?php
                if (isset($_POST['selectedImage'])) {
                    $hinhanh = $_POST['selectedImage'];
                    echo '<img src="' . $hinhanh . '" alt="Hình ảnh" style="max-width: 140px;" class="hinh">';
                }
                ?>
            </div>
        </div>

        <div class="b-c">
            <button class="b">Xác nhận</button>
        </div>
        <?php
        // Kết nối đến cơ sở dữ liệu
        include 'database.php';

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $name = $_POST["name"];
            $gender = $_POST["gender"];
            $department = $_POST["department"];
            $ngay_sinh = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['ngay-sinh'])));
            $dia_chi = $_POST["dia-chi"];
            $hinhanh = $_POST["selectedImage"]; // Đường dẫn hình ảnh

            // Thực hiện truy vấn SQL để chèn dữ liệu vào bảng students
            $sql = "INSERT INTO students (name, gender, department, ngay_sinh, dia_chi, hinhanh) VALUES ('$name', '$gender', '$department', '$ngay_sinh', '$dia_chi', '$hinhanh')";


            if (mysqli_query($conn, $sql)) {
                // Thành công
                echo "";
            } else {
                // Lỗi
                echo "Lỗi: " . $sql . "<br>" . mysqli_error($conn);
            }
        }
        ?>

    </form>
</body>

</html>
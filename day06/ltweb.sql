-- Create database ltweb
CREATE DATABASE ltweb;
USE ltweb;

-- Create table students
CREATE TABLE students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255),
    gender VARCHAR(10),
    department VARCHAR(255),
    ngay_sinh DATE,
    dia_chi VARCHAR(255),
    hinhanh BLOB
);

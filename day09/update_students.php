<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        form {
            width: 550px;
            padding: 40px;
            border: 2px solid rgb(48, 113, 178);
        }

        .c {
            display: flex;
            flex-wrap: wrap;
            gap: 20px;
            padding: 10px;
        }

        .l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 8px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
            text-align: center;
        }

        .i-c {
            flex: 2;
        }

        .i-t,
        .s {
            width: 100%;
            padding: 8px;
            margin-top: 5px;
        }

        .c-s {
            position: relative;
        }

        .c-s select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            width: 100%;
            padding: 10px;
            border: 2px solid rgb(48, 113, 178);
        }

        .a {
            position: absolute;
            top: 0;
            right: 0;
            width: 40px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: rgb(118, 178, 93);
            color: white;
            cursor: pointer;
        }

        .g-c {
            display: flex;
            gap: 20px;
            padding: 10px;
        }

        .g-l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 8px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            text-align: center;
        }

        .g-i-c {
            flex: 2;
            display: flex;
            align-items: center;
        }

        .g-i-c label {
            margin-right: 10px;
        }

        .b-c {
            text-align: center;
            margin-top: 20px;
        }

        .b {
            padding: 15px 50px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
            color: white;
            border: 2px solid rgb(48, 113, 178);
        }

        .b:hover {
            background-color: rgb(24, 87, 182);
        }

        .min {
            flex: 1;
            padding: 8px;
            height: 41px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .max {
            flex: 1;
            padding: 8px;
            width: 315px;
            height: 80px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .img {
            flex: 1.17;
        }

        .required {
            color: red;
        }

        .error-message {
            color: red;
        }

        .hinh {
            flex: 2;
            padding: 8px;
            float: left;
            margin-right: 175px;
        }
    </style>
</head>

<body>
    <?php
    include 'database.php';

    if (isset($_GET['id'])) {
        $studentId = $_GET['id'];

        $sql = "SELECT * FROM students WHERE id = $studentId";
        $result = mysqli_query($conn, $sql);

        if ($result && mysqli_num_rows($result) > 0) {
            $student = mysqli_fetch_assoc($result);

            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                $newName = $_POST['name'];
                $newGender = $_POST['gender'];
                $newDepartment = $_POST['department'];
                $newNgaySinh = $_POST['ngay_sinh'];
                $newDiaChi = $_POST['dia_chi'];
                $newHinhAnh = $_POST['selectedImage']; 

                $updateSql = "UPDATE students SET 
                    name = '$newName', 
                    gender = '$newGender', 
                    department = '$newDepartment',
                    ngay_sinh = '$newNgaySinh',
                    dia_chi = '$newDiaChi',
                    selectedImage = '$newHinhAnh'
                    WHERE id = $studentId";

                $updateResult = mysqli_query($conn, $updateSql);

                if ($updateResult) {
                    header("Location: list.php");
                    exit();
                } else {
                    echo 'Lỗi cập nhật thông tin sinh viên: ' . mysqli_error($conn);
                }
            }
        }
    }
    ?>

            <form method="POST">
                <div class="c">
                    <div class="l" style="max-width: 110px;">Họ và tên<span class="required">*</span></div>
                    <div class="i-c">
                        <input type="text" class="s" name="name" value="<?= $student['name'] ?>">
                    </div>
                </div>

                <div class="c">
                    <div class="l" style="max-width: 110px;">Giới tính<span class="required">*</span></div>
                    <div class="i-c">
                        <input type="text" class="s" name="gender" value="<?= $student['gender'] ?>" readonly>
                    </div>
                </div>

                <div class="c c-s">
                    <div class="l" style="max-width: 110px;">Phân khoa<span class="required">*</span></div>
                    <div class="i-c">
                        <input type="text" class="s" name="department" value="<?= $student['department'] ?>" readonly>
                    </div>
                </div>

                <div class="c">
                    <div class="l" style="max-width: 110px;">Ngày Sinh<span class="required">*</span></div>
                    <div class="i-c">
                        <input type="text" class="s" name="ngay_sinh" value="<?= $student['ngay_sinh'] ?>">
                    </div>
                </div>

                <div class="c">
                    <div class="l" style="max-width: 110px;">Địa chỉ</div>
                    <div class="i-c">
                        <input type="text" class="s" name="dia_chi" value="<?= $student['dia_chi'] ?>">
                    </div>
                </div>

                <div class="c">
                    <div class="img">
                        <div class="l" style="max-width: 110px;">Hình ảnh</div>
                    </div>
                    <div class="i-c">
                        <!-- <input type="hidden" name="selectedImage" value="<?= $student['selectedImage'] ?>"> -->
                    </div>
                </div>

                <div class="b-c">
                    <button class="b">Xác nhận</button>
                </div>
            </form>
            
    
</body>

</html>
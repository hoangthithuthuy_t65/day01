<?php
include 'database.php';

$department = isset($_POST['department']) ? $_POST['department'] : '';
$keyword = isset($_POST['keyword']) ? $_POST['keyword'] : '';

$sql = "SELECT * FROM students WHERE 1";

if (!empty($department)) {
    $sql .= " AND department = '$department'";
}

if (!empty($keyword)) {
    $sql .= " AND (name LIKE '%$keyword%' OR department LIKE '%$keyword%')";
}

$result = mysqli_query($conn, $sql);

if ($result) {
    $searchResults = mysqli_fetch_all($result, MYSQLI_ASSOC);
    echo json_encode($searchResults);
} else {
    echo json_encode(['error' => 'Lỗi truy vấn dữ liệu: ' . mysqli_error($conn)]);
}

mysqli_close($conn);
?>

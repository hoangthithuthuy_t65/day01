<?php
$host = "localhost"; // Địa chỉ máy chủ MySQL
$username = "root"; // Tên đăng nhập MySQL
$password = ""; // Mật khẩu MySQL
$database = "ltweb"; // Tên cơ sở dữ liệu

// Kết nối đến cơ sở dữ liệu
$conn = mysqli_connect($host, $username, $password, $database);

// Kiểm tra kết nối
if (!$conn) {
    die("Lỗi kết nối đến cơ sở dữ liệu: " . mysqli_connect_error());
}
<?php
// Include the database connection
include 'database.php';

// Get the department and keyword values from the AJAX request
$department = isset($_POST['department']) ? $_POST['department'] : '';
$keyword = isset($_POST['keyword']) ? $_POST['keyword'] : '';

// Construct the SQL query based on the provided department and keyword
$sql = "SELECT * FROM students WHERE 1";

if (!empty($department)) {
    $sql .= " AND department = '$department'";
}

if (!empty($keyword)) {
    $sql .= " AND (name LIKE '%$keyword%' OR department LIKE '%$keyword%')";
}

// Execute the query
$result = mysqli_query($conn, $sql);

if ($result) {
    // Fetch and return the search results in JSON format
    $searchResults = mysqli_fetch_all($result, MYSQLI_ASSOC);
    echo json_encode($searchResults);
} else {
    // Handle query errors
    echo json_encode(['error' => 'Lỗi truy vấn dữ liệu: ' . mysqli_error($conn)]);
}

// Close the database connection
mysqli_close($conn);
?>

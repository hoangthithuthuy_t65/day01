<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        .container {
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .header {
            display: flex;
            flex-direction: column;
            align-items: center;
            width: 100%;
            margin-top: 20px;
        }

        .search-bar {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .top-bar {
            display: flex;
            justify-content: space-between;
            align-items: center;
            width: 100%;
        }

        .department-bar {
            display: flex;
            align-items: center;
        }

        .keyword-bar {
            display: flex;
            align-items: center;
        }

        .department-select {
            height: 40px;
            width: 200px;
            border: 2px solid rgb(14, 75, 161);
            margin-left: 20px;
            margin-top: 10px;
        }

        .keyword-input {
            height: 40px;
            width: 200px;
            border: 2px solid rgb(14, 75, 161);
            margin-right: 50px;
            margin-top: 20px;
        }
        .i-s {
            margin-right: 10px;
            margin-top: 20px;
        }
        .i-ss {
            margin-right: 10px;
            margin-top: 20px;
            margin-right: 10px;
        }

        .search-button {
            background-color: rgb(48, 113, 178);
            color: white;
            border: 2px solid rgb(14, 75, 161);
            border-radius: 5px;
            padding: 5px 10px;
            cursor: pointer;
            margin-top: 20px;
        }

        .results {
            display: flex;
            justify-content: space-between;
            width: 100%;
            margin-top: 20px;
        }

        .student-count {
            display: flex;
            align-items: center;
            margin-top: 40px;
        }

        .add-button {
            background-color: rgb(48, 113, 178);
            color: white;
            height: 30px;
            border: 2px solid rgb(14, 75, 161);
            border-radius: 5px;
            padding: 1px 16px;
            cursor: pointer;
            margin-right: 280px;
            margin-top: 50px;
        }

        th {
            border: none;
            padding: 8px;
            text-align: left;
        }

        td {
            border: none;
            padding: 8px;
            text-align: left;
        }

        .action-button {
            background-color: rgb(48, 113, 178);
            color: white;
            border: 2px solid rgb(14, 75, 161);
            border-radius: 5px;
            padding: 5px 10px;
            cursor: pointer;
            text-align: center;
        }

        table {
            border: none;
            border-collapse: collapse;
            width: 100%;
        }

        .action-buttons {
            display: flex;
            gap: 10px;
        }

        .student-list {
            width: 100%;
            margin-top: 20px;
        }
        
    </style>
</head>

<body>
    <div class="container">
        <div class="header">
            <div class="search-bar">
                <div class="top-bar">
                    <div class="department-bar">
                        <label for="department-select" class="i-s">Khoa:</label>
                        <select id="department-select" class="department-select">
                            <option value="">—Chọn phân khoa—</option>
                            <option value="Khoa học máy tính">Khoa học máy tính</option>
                            <option value="Khoa học dữ liệu">Khoa học dữ liệu</option>
                        </select>
                    </div>
                </div>
                <div class="keyword-bar">
                    <label for="keyword-input" class="i-ss">Từ khóa:</label>
                    <input id="keyword-input" type="text" class="keyword-input" placeholder="Từ khóa">
                </div>
                <button id="resetButton" class="search-button">Reset</button>
            </div>
        </div>
        <?php
        include 'database.php';

        if (isset($_GET['department'])) {
            $selected_department = $_GET['department'];
            $sql = "SELECT * FROM students WHERE department = '$selected_department'";
        } else {
            $sql = "SELECT * FROM students";
        }

        $result = mysqli_query($conn, $sql);

        if ($result) {
            $num_students = mysqli_num_rows($result);
            echo '<div class="results">';
            echo '<p class="student-count">Số sinh viên tìm thấy: ' . $num_students . '</p>';
            echo '<button class="add-button" onclick="redirectToRegister()">Thêm</button>';
            echo '</div>';
        } else {
            echo 'Lỗi truy vấn dữ liệu: ' . mysqli_error($conn);
        }
        ?>
        <script>
            document.addEventListener("DOMContentLoaded", function () {
                document.getElementById("department-select").addEventListener("change", performSearch);
                document.getElementById("keyword-input").addEventListener("keyup", performSearch);

                document.getElementById("resetButton").addEventListener("click", resetSearch);

                function resetSearch() {
                    document.getElementById("department-select").value = "";
                    document.getElementById("keyword-input").value = "";

                    performSearch();
                }

                function performSearch() {
                    var departmentValue = document.getElementById("department-select").value;
                    var keywordValue = document.getElementById("keyword-input").value;

                    fetch('search.php', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: 'department=' + encodeURIComponent(departmentValue) + '&keyword=' + encodeURIComponent(keywordValue),
                    })
                        .then(handleResponse)
                        .then(updateStudentList)
                        .catch(handleError);
                }

                function updateStudentList(data) {
                    var tableBody = document.querySelector(".student-list tbody");
                    tableBody.innerHTML = "";

                    if (data.error) {
                        console.error('Error in search results:', data.error);
                    } else {
                        data.forEach(function (result, index) {
                            tableBody.innerHTML += `
                                <tr>
                                    <td>${index + 1}</td>
                                    <td>${result.name}</td>
                                    <td>${result.department}</td>
                                    <td>
                                        <div class="action-buttons">
                                            <button class="action-button edit-button" data-id="${result.id}">Sửa</button>
                                            <button class="action-button delete-button" data-id="${result.id}">Xóa</button>
                                        </div>
                                    </td>
                                </tr>`;
                        });
                    }
                }

                function redirectToRegister() {
                    window.location.href = "register.php";
                }

                function handleResponse(response) {
                    return response.json();
                }

                function handleError(error) {
                    console.error('Error during fetch:', error);
                }
            });
        </script>
        <div class="student-list">
            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tên Sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $row_number = 1;
                    while ($row = mysqli_fetch_assoc($result)) {
                        echo '<tr>';
                        echo '<td>' . $row_number . '</td>';
                        echo '<td>' . $row['name'] . '</td>';
                        echo '<td>' . $row['department'] . '</td>';
                        echo '<td>';
                        echo '<div class="action-buttons">';
                        echo '<button class="action-button edit-button" data-id="' . $row['id'] . '">Sửa</button>';
                        echo '<button class="action-button delete-button" data-id="' . $row['id'] . '">Xóa</button>';
                        echo '</td>';
                        echo '</tr>';
                        $row_number++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
5.1
CREATE DATABASE QLSV;
USE QLSV;

CREATE TABLE DMKHOA (
    MaKH varchar(6)PRIMARY KEY,
    TenKhoa varchar(30)
);

CREATE TABLE SINHVIEN (
    MaSV varchar(6)PRIMARY KEY,
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh DateTime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong Int,
    FOREIGN KEY (MaKH) REFERENCES DMKHOA(MaKH)
);

5.2
INSERT INTO DMKHOA (MaKH, TenKhoa)
VALUES ('MAKH01', 'công nghệ thông tin');

-- sql lay toan bo danh sach cac sinh vien thuoc khoa co ten la"Công nghê thông tin"
SELECT SINHVIEN.*
FROM SINHVIEN
INNER JOIN DMKHOA ON SINHVIEN.MaKH = DMKHOA.MaKH
WHERE DMKHOA.TenKhoa = 'công nghệ thông tin';
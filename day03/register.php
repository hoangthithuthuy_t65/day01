<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>bai3</title>
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        form {
            width: 550px;
            padding: 40px;
            border: 2px solid rgb(48, 113, 178);
        }

        .container {
            display: flex;
            flex-wrap: wrap;
            gap: 20px;
            padding: 10px;
        }

        .label {
            flex: 1;
            background-color: rgb(102, 153, 204);
            padding: 8px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .input-container {
            flex: 2;
        }

        input[type="text"],
        select {
            width: 100%;
            padding: 10px;
            border: 2px solid rgb(48, 113, 178);
            transition: border-color 0.3s;
        }

        input[type="text"]:hover,
        select:hover {
            border-color: rgb(102, 153, 204);
        }

        .custom-select {
            position: relative;
        }

        .custom-select select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            width: 100%;
            padding: 10px;
            border: 2px solid rgb(48, 113, 178);
        }

        .arrow {
            position: absolute;
            top: 0;
            right: 0;
            width: 40px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: rgb(102, 153, 204);
            color: white;
            cursor: pointer;
        }

        .gender-container {
            display: flex;
            gap: 20px;
            padding: 10px;
        }

        .gender-label {
            flex: 1;
            background-color: rgb(102, 153, 204);
            padding: 8px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            text-align: center;
        }

        .gender-input {
            flex: 2;
            display: flex;
            align-items: center;
        }

        .gender-input label {
            margin-right: 10px;
        }

        .button-container {
            text-align: center;
            margin-top: 20px;
        }

        .btn {
            padding: 15px 50px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
            color: white;
            border: 2px solid rgb(48, 113, 178);
        }

        .btn:hover {
            background-color: rgb(24, 87, 182);
        }
    </style>
    
</head>

<body>
    <form action="" class="bd-blue">
        <div class="container">
            <div class="label">Họ và tên</div>
            <div class="input-container">
                <input type="text" class="bd-blue" />
            </div>
        </div>

        <div class="container">
            <div class="label">Giới tính</div>
            <div class="input-container gender-container">
                <div class="gender-input">
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ');

                    foreach ($genders as $id => $name) {
                        echo '<input type="radio" id="' . $id . '" name="gender" value="' . $name . '" />';
                        echo '<label for="' . $id . '">' . $name . '</label>';
                    }
                    ?>

                </div>
            </div>
        </div>


        <div class="container">
            <div class="label">Phân khoa</div>
            <div class="input-container custom-select" style="width:50%;">
                <select class="bd-blue department-input" name="department">
                    <?php
                    $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học dữ liệu');

                    foreach ($departments as $id => $name) {
                        echo '<option value="' . $id . '">' . $name . '</option>';
                    }
                    ?>

                </select>
            </div>
        </div>


        <div class="button-container">
            <button type="submit" class="btn bd-blue bgblue text-white">Đăng ký</button>
        </div>
    </form>
</body>

</html>
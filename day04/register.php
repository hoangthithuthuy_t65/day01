<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>Form đăng ký</title>
    <style>
        * {
            box-sizing: border-box;
            border: none;
            outline: unset;
        }

        body {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
        }

        form {
            width: 550px;
            padding: 40px;
            border: 2px solid rgb(48, 113, 178);
        }

        .c {
            display: flex;
            flex-wrap: wrap;
            gap: 20px;
            padding: 10px;
        }

        .l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 8px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
            text-align: center;
        }

        .i-c {
            flex: 2;
        }

        .i-t,
        .s {
            width: 100%;
            padding: 10px;
            border: 2px solid rgb(48, 113, 178);
        }

        .c-s {
            position: relative;
        }

        .c-s select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            width: 100%;
            padding: 10px;
            border: 2px solid rgb(48, 113, 178);
        }

        .a {
            position: absolute;
            top: 0;
            right: 0;
            width: 40px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: rgb(118, 178, 93);
            color: white;
            cursor: pointer;
        }


        .g-c {
            display: flex;
            gap: 20px;
            padding: 10px;
        }

        .g-l {
            flex: 1;
            background-color: rgb(118, 178, 93);
            padding: 8px;
            color: white;
            border: 2px solid rgb(48, 113, 178);
            text-align: center;
        }

        .g-i-c {
            flex: 2;
            display: flex;
            align-items: center;
        }

        .g-i-c label {
            margin-right: 10px;
        }

        .b-c {
            text-align: center;
            margin-top: 20px;
        }

        .b {
            padding: 15px 50px;
            border-radius: 10px;
            cursor: pointer;
            background-color: rgb(103, 163, 93);
            color: white;
            border: 2px solid rgb(48, 113, 178);
        }

        .b:hover {
            background-color: rgb(24, 87, 182);
        }

        .min {
            flex: 1;
            padding: 8px;
            height: 41px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .max {
            flex: 1;
            padding: 8px;
            width: 315px;
            height: 80px;
            color: black;
            border: 2px solid rgb(48, 113, 178);
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .address {
            flex: 1.17;
        }

        .required {
            color: red;
        }

        .error-message {
            color: red;
        }
    </style>
</head>

<body>
    <form action="#" method="POST">
        <div class="error-message"></div>
        <div class="c">
            <div class="l" style="max-width: 110px;">Họ và tên<span class="required">*</span></div>
            <div class="i-c">
                <input type="text" class="s" name="name">
            </div>
        </div>

        <div class="c">
            <div class="l" style="max-width: 110px;">Giới tính<span class="required">*</span></div>
            <div class="i-c">
                <div class="g-i-c">
                    <?php
                    $genders = array(0 => 'Nam', 1 => 'Nữ');

                    foreach ($genders as $id => $name) {
                        echo '<input type="radio" id="' . $id . '" name="gender" value="' . $name . '" />';
                        echo '<label for="' . $id . '">' . $name . '</label>';
                    }
                    ?>
                </div>
            </div>
        </div>

        <div class="c c-s">
            <div class="l" style="max-width: 110px;">Phân khoa<span class="required">*</span></div>
            <div class="i-c">
                <select class="department-input" name="department" style="max-width: 183px;">
                    <option value="">—Chọn phân khoa—</option>
                    <?php
                    $departments = array('MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học dữ liệu');

                    foreach ($departments as $id => $name) {
                        echo '<option value="' . $id . '">' . $name . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>

        <div class="c">
            <div class="l" style="max-width: 110px;">Ngày Sinh<span class="required">*</span></div>
            <div class="i-c">
                <input type="text" class="min" id="ngay-sinh" name="ngay-sinh" placeholder="dd/mm/yyyy">
            </div>
        </div>

        <div class="c">
            <div class="address">
                <div class="l" style="max-width: 110px;">Địa chỉ</div>
            </div>
            <div class="i-c">
                <input type="text" class="max" name="dia-chi" />
            </div>
        </div>

        <div class="b-c">
            <button type="submit" class="b">Đăng ký</button>
        </div>
    </form>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            document.querySelector("form").addEventListener("submit", function(event) {
                var errors = [];
                var name = document.querySelector("input[name='name']").value;
                var gender = document.querySelector("input[name='gender']:checked");
                var department = document.querySelector(".department-input").value;
                var ngaySinh = document.getElementById("ngay-sinh").value;

                if (name === "") {
                    errors.push("Hãy nhập tên.");
                }

                if (!gender) {
                    errors.push("Hãy chọn giới tính.");
                }

                if (department === "") {
                    errors.push("Hãy chọn phân khoa.");
                }

                if (ngaySinh === "") {
                    errors.push("Hãy nhập ngày sinh.");
                } else {
                    var dateRegex = /^(0[1-9]|[12][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/;;
                    if (!dateRegex.test(ngaySinh)) {
                        errors.push("Hãy nhập ngày sinh đúng định dạng.");
                    }
                }

                if (errors.length > 0) {
                    event.preventDefault();
                    var errorMessage = errors.join("<br>");
                    document.querySelector(".error-message").innerHTML = errorMessage;
                    document.querySelector(".error-message").style.display = "block";

                }
            });
        });
    </script>
</body>

</html>